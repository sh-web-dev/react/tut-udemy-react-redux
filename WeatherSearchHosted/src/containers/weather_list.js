import React, { Component} from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart';
import GoogleMaps from '../components/google_map';

class WeatherList extends Component {
    renderWeather(cityData){
        const cityName = cityData.city.name;
        const temps = cityData.list.map(weather => weather.main.temp);
        const pressure = cityData.list.map(weather => weather.main.pressure);
        const humidity = cityData.list.map(weather => weather.main.humidity);
        const { lon, lat } = cityData.city.coord;   //fetches values of lon and lat from cityData.city.coord and assigns to constants lon and lat.
        return(
            <tr key={cityName}>
                <td><GoogleMaps lon={lon} lat={lat} /></td>
                <td><Chart data={temps} color="orange" units="K"/></td>
                <td><Chart data={pressure} color="green" units="hPa"/></td>
                <td><Chart data={humidity} color="black" units="%"/></td>
             </tr>
        );
    }

    render(){
        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                    <th>City</th>
                    <th>Temperature (K)</th>
                    <th>Pressure (hPa)</th>
                    <th>Humidity (%)</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.weather.map(this.renderWeather)}
                </tbody>
            </table>
        );
    }
}

// as we need only weather property from the state we can use es6 syntax by {weather} which is similar to state.weather
function mapStateToProps({ weather }){
    // as the key and value is having the same name just giving one is enough in es6
    return { weather };
}

export default connect(mapStateToProps)(WeatherList);