import React from 'react';
import VideoListItem from './video_list_item';

const VideoList = (props)=>{
    const videoItems = props.videos.map((video) => {
        //In a list each item should have an id(key) so that in future we can alter them individually
        return (
        <VideoListItem 
        key={video.etag}
        video={video}
        onVideoSelect={props.onVideoSelect} />
    );
    });
    return(
        <ul className="col-md-4 list-group">
            { videoItems }
        </ul>
    );
};

export default VideoList;