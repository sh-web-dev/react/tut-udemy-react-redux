import React, {Component} from 'react';
import ReactDOM from 'react-dom';
//React hadles components and ReactDOM handles displaying them on the DOM

//loadsh _ module
import _ from 'loadsh';

//Youtube search module
import YTSearch from 'youtube-api-search';

//Importing components. We need to provide a path reference for importing the files written by us.
import SearchBar from './components/search_bar';
import VideosList from './components/video_list';
import VideoDetail from './components/video_detail';

const API_KEY = 'AIzaSyA_JON_KdoQbMoLrNQrWOj-AkIXTmPmZGU';

//Create a new component. This component should produce some HTML. There will only be one component per js file.
class App extends Component{
    constructor(props){
        super(props);

        this.state = { videos: [], 
            selectedVideo: null
        };
        this.videoSearch('frozen');   
    }
    videoSearch(term){
        YTSearch({ key: API_KEY, term: term }, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
            //{videos} is same as {videos: videos}
        });
    }
render() {
    const videoSearch = _.debounce((term) => { this.videoSearch(term) }, 600);
    return ( 
        <div>
            {/* SearchBar have to call props.onSearchTermChange with new search term and that will call the videoSearch*/}
            <SearchBar onSearchTermChange={videoSearch}/>
            <VideoDetail video={this.state.selectedVideo}/>
            <VideosList 
                onVideoSelect={ selectedVideo => this.setState({selectedVideo}) }
                videos={ this.state.videos }/>
        </div>
    );
    //sending videos to VideosList is called props. In functional components props is arguements
    //In class components props are available in any method as this.props
}
}

//App is like a class we need to create its instance. Wrap the class name in JSX tag to instantiate it. eg: <App/>

//Take this component's generated HTML and put it on the DOM
ReactDOM.render(<App />, document.querySelector('.container'));
