import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fetchWeather} from '../actions/index';

class SearchBar extends Component {
    constructor(props){
        super(props);
        //We have to set state whenever the input is changed. as searchbar is controlled component
        this.state = {term: ''};

        //Binding the context with onInputChange
        /* 
            this, which is the instance of searchbar has a function called onInputChange and bind that function
            to search bar and replace existing it with the bound function
        */
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onInputChange(event){
        this.setState({term: event.target.value})
    }
    onFormSubmit(event){
        event.preventDefault();

        // fetch weather data.
        this.props.fetchWeather(this.state.term);
        this.setState({term: ''});
    }

    render() {
        return(
            <form onSubmit={this.onFormSubmit} className="input-group">
            {/* We need to update the state using change handler on the input */}
                <input
                placeholder="Get a 5 day forecast for your favourite cities."
                className="form-control"
                value={this.state.term}
                onChange={this.onInputChange} />
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">Submit</button>
                </span>
            </form>
        
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({fetchWeather},dispatch);
}

export default connect(null, mapDispatchToProps)(SearchBar);