import React, { Component } from 'react';

class GoogleMaps extends Component {
    //google.maps object can be used to work with the google map api
    componentDidMount(){
        // this.refs.map is the reference of the div whose reference is set as 'map'
        new google.maps.Map(this.refs.map, {
            zoom: 12,
            center: {
                lat: this.props.lat,
                lng: this.props.lon
            }
        });
    }

    render(){
        // ref will provide a direct reference to the html element. here map is reference for the div
        return <div ref="map" />;
    }

    /**
     * import {GoogleMapLoader, GoogleMap} from 'react-google-maps';
     * We can also do it as follows:
     * export default (props) => {
     *  return (
     *      <GoogleMapLoader
     *          containerElement={<div style={{height:100%}} />}
     *          googleMapElement={
     *              <GoogleMap defaultZoom={12} defaultCenter={{lat:props.lat, lng:props.lon}}/>
     *          }
     *  );
     * }
     */
}

export default GoogleMaps;