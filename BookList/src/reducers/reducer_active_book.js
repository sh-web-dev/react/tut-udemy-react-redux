//state : not application state but the piece of state that this reducer is responsible for.
//reducer is called whenever the state is changed
//reducers cannot return undefined so we initialize it to null if it doesn't exist.
export default function(state = null, action){
    switch(action.type) {
        //If the action that we are concerned with is encounterd we will return the new value of the piece of state.
        //We will not mutate the current state. We will only return a fresh piece of state.
        case 'BOOK_SELECTED':
            return action.payload;
        //When our app first starts up the state is null. So to handle it in the render method we add a check.
    }
    return state;
}
