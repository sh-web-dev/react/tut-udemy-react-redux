import React from 'react';
import {Component} from 'react';
import {connect} from 'react-redux';

class BookDetail extends Component{
    render(){
        //When our app first starts up the state is null. So to handle it in the render method we add a check whether the props value is null or not.
        if( !this.props.book ){
            return <div>Select a book to continue.</div>
        }
        return (
            <div>
                <h3>Details for:</h3>
                <div>{this.props.book.title}</div>
                <h4>Pages: </h4>

                <div>{this.props.book.pages}</div>
            </div>
        );
    } 
}
function mapStateToProps(state) {
    return {
        book: state.activeBook
    };
}
export default connect(mapStateToProps)(BookDetail);